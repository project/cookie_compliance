<?php

declare(strict_types=1);

namespace Drupal\cookie_compliance\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Cookie Compliance settings for this site.
 */
final class CookieComplianceSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'cookie_compliance_cookie_compliance_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['cookie_compliance.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['details'] = [
      '#type' => 'markup',
      '#markup' => 'Please register <a href="https://cookie-compliance.co">here</a>, add a domain and go to configuration -&gt; setup to get App ID and App secret.',
    ];

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable cookie compliance banner'),
      '#default_value' => $this->config('cookie_compliance.settings')->get('enabled'),
    ];

    $form['app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App ID'),
      '#default_value' => $this->config('cookie_compliance.settings')->get('app_id'),
    ];

    $form['app_secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App Secret Key'),
      '#default_value' => $this->config('cookie_compliance.settings')->get('app_secret_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if (!empty($form_state->getValue('app_id')) && !preg_match("/^[a-z0-9\-]+$/i", $form_state->getValue('app_id'))) {
      $form_state->setErrorByName(
        'app_id',
        $this->t('App ID should contain only lower case alpha numeric and a hyphen'),
      );
    }
    if (!empty($form_state->getValue('app_secret_key')) && !preg_match("/^[a-z0-9\-]+$/i", $form_state->getValue('app_secret_key'))) {
      $form_state->setErrorByName(
        'app_secret_key',
        $this->t('App Secret Key should be alphanumeric only'),
      );
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('cookie_compliance.settings')
      ->set('app_id', $form_state->getValue('app_id'))
      ->set('app_secret_key', $form_state->getValue('app_secret_key'))
      ->set('enabled', $form_state->getValue('enabled'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
